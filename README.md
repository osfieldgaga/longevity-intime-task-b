# Longevity InTime Test Task B

This is the repository for the flutter app of task B.

The prototype of the app can be found here:
https://www.figma.com/proto/jANZukzpJV6NOxfmlHZMnj/Longevity-InTime-Test-Task?page-id=1%3A30&node-id=1%3A31&viewport=510%2C589%2C1.02&scaling=scale-down&starting-point-node-id=1%3A31

The APK file can be found here:
https://drive.google.com/file/d/13dL68MgEifTC88LrNnPwV-8EXiQDs2H8/view?usp=sharing

The data models was collected directly from the provided Google Sheet form using Opensheet API:
 https://benborgers.com/posts/google-sheets-json

The data was obtain in the JSON format.
