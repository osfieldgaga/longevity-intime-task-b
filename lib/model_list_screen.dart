import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:longevity_intime_task_b/design_system_const.dart';
import 'package:longevity_intime_task_b/model_details.dart';

class ModelListScreen extends StatefulWidget {
  const ModelListScreen({super.key, required this.modelData});

  final dynamic modelData;

  @override
  State<ModelListScreen> createState() => _ModelListScreenState();
}

class _ModelListScreenState extends State<ModelListScreen> {

TextEditingController textEditingController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.modelData);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: grayDark),
        centerTitle: true,
        title: Text(
          "Models",
          style: bodyBold.copyWith(color: grayDark),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        toolbarHeight: 64,
        bottom: PreferredSize(
            child: Container(
              color: grayLight,
              height: 1.0,
            ),
            preferredSize: Size.fromHeight(4.0)),
      ),
      body: Column(children: <Widget>[
          const SizedBox(
          height: spacingNormal,
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return MaterialButton(
                child: ListTile(
                  title: Text(
                    widget.modelData[index]['Model'].toString(),
                    style: bodyRegular,
                  ),
                ),
                onPressed: (() {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ModelDetailsScreen(
                        title: widget.modelData[index]['Model'].toString(),
                        numberOfMarker: widget.modelData[index]['Number of markers'].toString(),
                        accuracy: widget.modelData[index]['Accuracy'].toString(),
                        markers: widget.modelData[index]['List of markers'].toString(),
                      ),
                    ),
                  );
                }),
              );
            },
            itemCount: 37,
          ),
        )
      ]),
    );
  }
}
