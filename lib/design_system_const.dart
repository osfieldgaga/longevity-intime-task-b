import 'package:flutter/material.dart';

//Spacings/Distances
const paddingNormal = 24.0;
const spacingSmall = 4.0;
const spacingNormal = 8.0;
const spacingMedium = 12.0;
const spacingXMedium = 16.0;
const spacingLarge = 24.0;
const spacingXLarge = 32.0;
const spacingXXLarge = 40.0;
const spacingGap = 48.0;
const spacingXGap = 64.0;

//Colors
Color primary = new Color(0xFF3683FC);
Color grayLight = new Color(0xFFF1F2F3);
Color gray = new Color(0xFF768398);
Color grayDark = new Color(0xFF495261);

//Font Styles
const TextStyle heading1 = TextStyle(
  fontFamily: 'RealText',
  fontSize: 24,
  fontWeight: FontWeight.w700,
  //height: 24,
);

const TextStyle bodyBold = TextStyle(
  fontFamily: 'RealText',
  fontSize: 16,
  fontWeight: FontWeight.w700,
  //height: 24,
);

const TextStyle bodyRegular = TextStyle(
  fontFamily: 'RealText',
  fontSize: 16,
  fontWeight: FontWeight.w400,
  //height: 24,
);

const TextStyle small = TextStyle(
  fontFamily: 'RealText',
  fontSize: 14,
  fontWeight: FontWeight.w400,
  //height: 16,
);

const TextStyle smallLight = TextStyle(
  fontFamily: 'RealText',
  fontSize: 14,
  fontWeight: FontWeight.w300,
  //height: 16,
);