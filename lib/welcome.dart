import 'package:flutter/material.dart';
import 'package:longevity_intime_task_b/model_list_screen.dart';
import 'design_system_const.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'model_data_json.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  bool isDataReady = false;
  var dataToPass;

  Future<void> getModelData() async {
    var emptyMap = {};
    Uri uriOriginal = Uri.parse(
        'https://opensheet.elk.sh/1VJX1pdlOtEypAqTc7zomtBQSiOarbEW3HgCYeC7PswM/%D0%9B%D0%B8%D1%81%D1%821');
    Response response = await get(uriOriginal);
    var modelData = jsonDecode(response.body);

    //;Map model_data_local = jsonDecode(model_data_json);

    if (response.statusCode == 200) {
      print("Model data:\n");
      print(modelData[0]['Model']);
      setState(() {
        isDataReady = true;
        print("data ready? ");
        print(isDataReady);
      });

      dataToPass = modelData;
    } else {
      print("Model data fetch failed");
      
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getModelData();
    print("data ready? ");
    print(isDataReady);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: paddingNormal),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Welcome to this demo',
                style: bodyBold.copyWith(color: grayDark),
              ),
              const SizedBox(
                height: spacingNormal,
              ),
              Text(
                'You can go through the list of model and tap on any of them to have more info.',
                style: bodyRegular.copyWith(
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: spacingGap,
              ),
              ElevatedButton(
                onPressed: () {
                  if (isDataReady) {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ModelListScreen(
                          modelData: dataToPass,
                        ),
                      ),
                    );
                  }
                },
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      //side: BorderSide(color: Colors.red),
                    ),
                  ),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 40),
                  child: isDataReady
                      ? Text(
                          'See Models',
                          style: bodyBold.copyWith(color: Colors.white),
                        )
                      : CircularProgressIndicator(
                          color: Colors.white,
                        ),
                ),
              ),
              const SizedBox(
                height: spacingNormal,
              ),
              Text(
                isDataReady ? "Data loaded!" : "Loading data",
                style: smallLight,
              )
            ],
          ),
        ),
      ),
    );
    ;
  }
}
