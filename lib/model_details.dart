import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'design_system_const.dart';

class ModelDetailsScreen extends StatefulWidget {
  const ModelDetailsScreen(
      {super.key,
      required this.title,
      required this.markers,
      required this.numberOfMarker,
      required this.accuracy});

  final title, markers, numberOfMarker, accuracy;

  @override
  State<ModelDetailsScreen> createState() => _ModelDetailsScreenState();
}

class _ModelDetailsScreenState extends State<ModelDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: grayDark),
        centerTitle: true,
        title: Text(
          widget.title,
          style: bodyBold.copyWith(color: grayDark),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        toolbarHeight: 64,
        bottom: PreferredSize(
            child: Container(
              color: grayLight,
              height: 1.0,
            ),
            preferredSize: Size.fromHeight(4.0)),
      ),
      body: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: grayLight,
                    borderRadius: BorderRadius.all(Radius.circular(32.0))),
                height: 128,
                width: 128,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          "Number of \nmarkers",
                          style: smallLight,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: spacingMedium,
                      ),
                      Text(
                        widget.numberOfMarker.toString(),
                        style: bodyBold.copyWith(color: grayDark),
                      ),
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                    color: grayLight,
                    borderRadius: BorderRadius.all(Radius.circular(32.0))),
                height: 128,
                width: 128,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          "Accurarcy",
                          style: smallLight,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: spacingMedium,
                      ),
                      Text(
                        widget.accuracy.toString(),
                        style: bodyBold.copyWith(color: grayDark),
                      ),
                    ]),
              )
            ],
          ),
        ),
        const SizedBox(height: spacingXLarge,),
        const Text("List of markers", style: bodyBold,),
        const SizedBox(height: spacingNormal,),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Text(widget.markers, style: bodyRegular,),
        ),
      ]),
    );
  }
}
